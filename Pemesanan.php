<?php
require 'Function.php';
session_start();

if(!isset($_SESSION["login"])) {
    header("Location: Login.php");
    exit;
}

if (isset($_POST["pesan"])) {
    if (!pemesanan($_POST) > 0) {
        echo mysqli_error($connection);
    }
    
    $pemesanan = query("SELECT * FROM pemesanan");
    foreach ($pemesanan as $row) :
    ?>
    <div
        class="col-5 card rounded-5 offset-4 mt-5"
        style="background-color: #5d61bc"
    >
        <h2 class="text-white fw-bold pt-4 ps-2">Status Pembayaran</h2>
        <p class="pt-4 ps-2 fw-bold text-white fs-4">Dari</p>
        <p class="ps-2 text-white" style="margin-top: -15px" id="dariPembayaran">
        <?= $row["kota_asal"]?>
        </p>
        <p class="pt-1 ps-2 fw-bold text-white fs-4">Tujuan</p>
        <p class="ps-2 text-white" style="margin-top: -15px" id="tujuanPembayaran">
        <?= $row["kota_tujuan"]?>
        </p>
        <p class="pt-1 ps-2 fw-bold text-white fs-4">Tanggal Pergi</p>
        <p
        class="ps-2 text-white"
        style="margin-top: -15px"
        id="tanggalPergiPembayaran"
        >
        <?= $row["tanggal_pergi"]?>
        </p>
        <p class="pt-1 ps-2 fw-bold text-white fs-4">Jumlah Kursi</p>
        <p class="ps-2 text-white" style="margin-top: -15px" id="jumlahKursiPembayaran">
        <?= $row["jumlah_kursi"]?>
        </p>
        <p class="pt-1 ps-2 fw-bold text-white fs-4">Harga</p>
        <p class="ps-2 text-white" style="margin-top: -15px" id="hargaPembayaran">
        <?= $row["harga"]?>
        </p>
        <p class="pt-1 ps-2 fw-bold text-white fs-4">Status Pembayaran</p>
        <p class="ps-2 text-white pb-5" style="margin-top: -15px">Lunas</p>
    </div>
    <?php
    endforeach;
}
?>