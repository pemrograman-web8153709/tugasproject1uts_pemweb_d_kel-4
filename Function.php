<?php
$connection = mysqli_connect("localhost", "root", "rahasia", "uts_pemweb");

function query($query) {
    global $connection;
    $result = mysqli_query($connection, $query);
    $rows = [];
    while($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function registrasi($data) {
    global $connection;

    $nama_lengkap = strtolower(stripslashes($data["nama_lengkap"]));
    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($connection, $data["password"]);
    $password2 = mysqli_real_escape_string($connection, $data["password2"]);
    
    $result = mysqli_query($connection, "SELECT username FROM users WHERE username = '$username'");

    if(mysqli_fetch_assoc($result)){
        echo "<script>
            alert('Username Sudah Terdaftar!');
            </script>";
            return false;
    }

    if($password !== $password2) {
        echo "<script>
            alert('Konfirmasi Password Tidak Sesuai!');
            </script>";
            return false;
    }

    $password = password_hash($password, PASSWORD_DEFAULT);

    mysqli_query($connection, "INSERT INTO  users VALUES('', '$nama_lengkap', '$username', '$password')");
    return mysqli_affected_rows($connection);

}

function pemesanan($data) {
    global $connection;

    $kotaAsal = $data["kotaAsal"];
    $kotaTujuan =  $data["kotaTujuan"];
    $alamatPenjemputan = $data["alamatPenjemputan"];
    $alamatTujuan = $data["alamatTujuan"];
    $tanggalPergi = $data["tanggalPergi"];
    $jumlahKursi = $data["jumlahKursi"];
    $harga = $data["jumlahKursi"] * 140000;
    $buktiTransfer = upload();

    if(!$buktiTransfer){
        return false;
    }

    mysqli_query($connection, "INSERT INTO  pemesanan VALUES('', '$kotaAsal', '$kotaTujuan', '$alamatPenjemputan', '$alamatTujuan', '$tanggalPergi', '$jumlahKursi', '$harga', '$buktiTransfer')");
    return mysqli_affected_rows($connection);
}

function upload(){
    $namaFile = $_FILES['buktiTransfer']['name'];
    $ukuranFile = $_FILES['buktiTransfer']['size'];
    $error = $_FILES['buktiTransfer']['error'];
    $tmpName = $_FILES['buktiTransfer']['tmp_name'];

    if($error === 4){
        echo "<script>
            alert('Upload Bukti TransferTterlebih Dahulu!');
            </script>";
            return false;
    }

    $ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    if(!in_array($ekstensiGambar, $ekstensiGambarValid)){
        echo "<script>
            alert('Yang Anda Upload Bukan Gambar');
            </script>";
            return false;
    }

    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    move_uploaded_file($tmpName, 'BuktiTransfer/'. $namaFileBaru);
    return $namaFileBaru;
}